using System.Collections;
using System.Collections.Generic;
using System.IO;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using tvx.Clicker;
using UnityEngine;

public class Test : MonoBehaviour
{
    private SinCityClient client;
    private LoginResponse responce;
    
    // Start is called before the first frame update
    void Start()
    {
        client = new SinCityClient();
        client.Login("");
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(client.);
    }
}

public class SinCityClient {
    private readonly string server = "clicker.msg.nostress.dev:443";
    private readonly ClickerBackend.ClickerBackendClient client;
    private readonly Channel channel;

    public SinCityClient() {
        channel = new Channel(server, new SslCredentials());
        client = new ClickerBackend.ClickerBackendClient(channel);
    }

    public LoginResponse Login(string email) {
        var request = new LoginRequest {Email = email};
        return client.Login(request);
    }

    private void OnDisable() {
        channel.ShutdownAsync().Wait();
    }
}
